package com.horizon.zero.dawn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HorizonZeroDawnApplication {

    public static void main(String[] args) {
        SpringApplication.run(HorizonZeroDawnApplication.class, args);
    }
}
