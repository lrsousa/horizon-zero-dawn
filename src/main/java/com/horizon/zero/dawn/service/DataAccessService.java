package com.horizon.zero.dawn.service;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.WRITE;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class DataAccessService {
    
    @Autowired
    Environment environment;
    
    private final OpenOption[] options = new OpenOption[] { WRITE, CREATE, APPEND };
    
    public void write(String line) {
        try {
            Files.write(this.getResourceDirectory(), line.concat(System.lineSeparator()).getBytes(), options);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public String read(String key) {
        String line = null;
        if(Files.exists(this.getResourceDirectory())) {
            try (Stream<String> stream = Files.lines(this.getResourceDirectory())) {
                line = stream.filter(x -> x.split(";")[0].equals(key))
                        .findFirst()
                        .get();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return line;
        }
        return null;
    }
    
    private Path getResourceDirectory() {
        String fileName = environment.getProperty("local.server.port").concat("-data-file.kenai");
        return Paths.get("/home/leonardo-rangel-sousa/Documents/HorizonZeroDawn", fileName); 
    }
}
