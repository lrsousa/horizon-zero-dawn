package com.horizon.zero.dawn.service;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.horizon.zero.dawn.model.dto.PersonDTO;
import com.horizon.zero.dawn.model.dto.ServerConfigurationDTO;

@Service
public class PersonService {

    @Autowired
    private DataAccessService dataAccessService;
    
    @Autowired
    private ServerConfigurationDTO serverConfiguration;
    
    @Autowired
    Environment environment;
    
    public void save(PersonDTO person) {
        this.dataAccessService.write(person.toString());
        Integer port = Integer.valueOf(environment.getProperty("local.server.port"));
        if(!Arrays.asList(serverConfiguration.getRedundancePorts()).stream().anyMatch(port::equals)) {
            String uri = "http://localhost:".concat(String.valueOf(port + 10))
                                            .concat("/api/horizon-zero-dawn/person/save");
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.postForObject(uri, person, PersonDTO.class);
        }
    }
    
    public PersonDTO find(String email) {
//    	if(!email.toUpperCase().matches("^[A-H].*")) {
//    		throw new RuntimeException("This email is out of range.");
//    	}
        String line = this.dataAccessService.read(email);
        return this.lineToPerson(line);
    }
    
    private PersonDTO lineToPerson(String line) {
        String[] splitedLine = line.split(";");
        return PersonDTO.create()
                        .withEmail(splitedLine[0])
                        .withName(splitedLine[1])
                        .withLastName(splitedLine[2])
                        .withAge(null != splitedLine[3] ? Integer.parseInt(splitedLine[3]) : null)
                        .withPhone(splitedLine[4]);
    }
}
