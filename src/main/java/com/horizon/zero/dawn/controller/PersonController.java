package com.horizon.zero.dawn.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.horizon.zero.dawn.model.dto.PersonDTO;
import com.horizon.zero.dawn.service.PersonService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("person")
@Api(value = "person_controller", tags="person")
public class PersonController {

    @Autowired
    private PersonService personService;
    
    @PostMapping(value="/save", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "save")
    public ResponseEntity<Void> save(@ApiParam(name = "Person", value = "person", required = true)
                               @RequestBody(required = true)
                               @Valid PersonDTO person) {
        this.personService.save(person);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
    
    @GetMapping("/find/{email:.+}")
    @ApiOperation(value = "find one")
    public ResponseEntity<PersonDTO> find(@ApiParam(name = "email", value = "email", required = true)
                                          @PathVariable(value = "email", required = true) String email) {
        return ResponseEntity.status(HttpStatus.FOUND)
                             .body(this.personService.find(email));
    
    }
}
