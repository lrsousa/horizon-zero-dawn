package com.horizon.zero.dawn.model.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class PersonDTO {
    
    @NotBlank(message="Name não pode ser nulo ou vazio!")
    @Size(min = 1, max = 55, message="name deve conter entre 1 e 55 caracteres!")
    String name;
    @NotBlank(message="Sobrenome não pode ser nulo ou vazio!")
    @Size(min = 1, max = 200, message="Sobrenome deve conter entre 1 e 200 caracteres!")
    String lastName;
    @Min(value=0, message="Idade deve ser um avalor entre 1 e 120." )
    @Max(value=120, message="Idade deve ser um avalor entre 1 e 120.")
    Integer age;
    String phone;
    @NotBlank(message="Email não pode ser nulo ou vazio!")
    @Size(min = 6, max = 255, message="Email deve conter entre 6 e 255 caracteres!")
    String email;

    public static synchronized PersonDTO create() {
        return new PersonDTO();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PersonDTO withName(String name) {
        this.name = name;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public PersonDTO withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public PersonDTO withAge(Integer age) {
        this.age = age;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public PersonDTO withPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public PersonDTO withEmail(String email) {
        this.email = email;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        return sb.append(email).append(";")
                 .append(name).append(";")
                 .append(lastName).append(";")
                 .append(age).append(";")
                 .append(phone).append(";")
                 .toString();
    }
}
