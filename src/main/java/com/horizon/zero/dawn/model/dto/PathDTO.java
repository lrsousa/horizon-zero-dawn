package com.horizon.zero.dawn.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class PathDTO {

    private String port;
    private String domain;
    private String contextPath;
    
    public static synchronized PathDTO create() {
        return new PathDTO();
    }
    public String getPort() {
        return port;
    }
    public void setPort(String port) {
        this.port = port;
    }
    public PathDTO withPort(String port) {
        this.port = port;
        return this;
    }
    public String getDomain() {
        return domain;
    }
    public void setDomain(String domain) {
        this.domain = domain;
    }
    public PathDTO withDomain(String domain) {
        this.domain = domain;
        return this;
    }
    public String getContextPath() {
        return contextPath;
    }
    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }
    public PathDTO withContextPath(String contextPath) {
        this.contextPath = contextPath;
        return this;
    }
	@Override
	public String toString() {
		return domain + ":" + port + contextPath + "/";
	}
    
}
