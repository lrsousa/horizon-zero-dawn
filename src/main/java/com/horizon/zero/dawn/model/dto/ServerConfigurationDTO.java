package com.horizon.zero.dawn.model.dto;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("path.server")
public class ServerConfigurationDTO {
    
    @Value("#{'${path.server.redundancePorts}'.split(',')}")
    private Integer[] redundancePorts;

    public Integer[] getRedundancePorts() {
        return redundancePorts;
    }

    public void setRedundancePorts(Integer[] redundancePorts) {
        this.redundancePorts = redundancePorts;
    }
}
